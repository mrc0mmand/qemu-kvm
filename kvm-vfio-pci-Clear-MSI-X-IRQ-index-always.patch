From 43236995e8ad336d366b625fb8362046be53fc34 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?C=C3=A9dric=20Le=20Goater?= <clg@redhat.com>
Date: Mon, 29 Jan 2024 09:46:34 +0100
Subject: [PATCH] vfio/pci: Clear MSI-X IRQ index always
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

RH-Author: Cédric Le Goater <clg@redhat.com>
RH-MergeRequest: 218: vfio/pci: Clear MSI-X IRQ index always
RH-Jira: RHEL-21293
RH-Acked-by: Eric Auger <eric.auger@redhat.com>
RH-Acked-by: Alex Williamson <None>
RH-Commit: [1/1] b4b587b13c11e350d3e5fcc11ba66a006b25a763 (clegoate/qemu-kvm-c9s)

JIRA: https://issues.redhat.com/browse/RHEL-21293

commit d2b668fca5652760b435ce812a743bba03d2f316
Author: Cédric Le Goater <clg@redhat.com>
Date:   Thu Jan 25 14:27:36 2024 +0100

    vfio/pci: Clear MSI-X IRQ index always

    When doing device assignment of a physical device, MSI-X can be
    enabled with no vectors enabled and this sets the IRQ index to
    VFIO_PCI_MSIX_IRQ_INDEX. However, when MSI-X is disabled, the IRQ
    index is left untouched if no vectors are in use. Then, when INTx
    is enabled, the IRQ index value is considered incompatible (set to
    MSI-X) and VFIO_DEVICE_SET_IRQS fails. QEMU complains with :

    qemu-system-x86_64: vfio 0000:08:00.0: Failed to set up TRIGGER eventfd signaling for interrupt INTX-0: VFIO_DEVICE_SET_IRQS failure: Invalid argument

    To avoid that, unconditionaly clear the IRQ index when MSI-X is
    disabled.

    Buglink: https://issues.redhat.com/browse/RHEL-21293
    Fixes: 5ebffa4e87e7 ("vfio/pci: use an invalid fd to enable MSI-X")
    Cc: Jing Liu <jing2.liu@intel.com>
    Cc: Alex Williamson <alex.williamson@redhat.com>
    Reviewed-by: Alex Williamson <alex.williamson@redhat.com>
    Signed-off-by: Cédric Le Goater <clg@redhat.com>

Signed-off-by: Cédric Le Goater <clg@redhat.com>
---
 hw/vfio/pci.c | 8 +++++---
 1 file changed, 5 insertions(+), 3 deletions(-)

diff --git a/hw/vfio/pci.c b/hw/vfio/pci.c
index adb7c09367..29bb8067eb 100644
--- a/hw/vfio/pci.c
+++ b/hw/vfio/pci.c
@@ -829,9 +829,11 @@ static void vfio_msix_disable(VFIOPCIDevice *vdev)
         }
     }
 
-    if (vdev->nr_vectors) {
-        vfio_disable_irqindex(&vdev->vbasedev, VFIO_PCI_MSIX_IRQ_INDEX);
-    }
+    /*
+     * Always clear MSI-X IRQ index. A PF device could have enabled
+     * MSI-X with no vectors. See vfio_msix_enable().
+     */
+    vfio_disable_irqindex(&vdev->vbasedev, VFIO_PCI_MSIX_IRQ_INDEX);
 
     vfio_msi_disable_common(vdev);
     vfio_intx_enable(vdev, &err);
-- 
2.39.3

